import React, { useState, useEffect } from 'react'
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  FlatList,
} from 'react-native'
import { useNavigation, useRoute } from '@react-navigation/native'
import Axios from 'axios'
import { TextInput } from 'react-native-gesture-handler'

export default function Home() {
  const [users, setUsers] = useState([])
  const route = useRoute()

  useEffect(() => {
    async function loadUsers() {
      const { data } = await Axios.get('http://192.168.100.156:3000/users')
      setUsers(data)
    }

    loadUsers()
  }, [route.params?.data])

  const navigation = useNavigation()

  return (
    <View style={styles.container}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: '100%',
          marginBottom: 20,
        }}
      >
        <Text style={{ fontSize: 16 }}>Lista de Usuarios</Text>

        <TouchableOpacity onPress={() => navigation.navigate('Cadastrar')}>
          <Text style={{ fontSize: 14, color: 'blue', fontWeight: '500' }}>
            Adicionar Usuario
          </Text>
        </TouchableOpacity>
      </View>

      <TextInput style={styles.textInput} placeholder="Encontrar usuario" />

      <FlatList
        style={styles.imageView}
        keyExtractor={(item, index) => item.id.toString()}
        data={users}
        renderItem={({ item }) => (
          <TouchableOpacity style={styles.containerProduct}>
            <View style={{ flexDirection: 'column' }}>
              <Text style={styles.txtTitle}>{item.name}</Text>
              <Text style={styles.txtInput}>Nome: {item}</Text>
            </View>
            <View style={{ flexDirection: 'column' }}></View>
          </TouchableOpacity>
        )}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtTitle: {
    fontSize: 16,
    marginBottom: 3,
    marginTop: 5,
  },
  txtInput: {
    fontSize: 12,
    marginBottom: 3,
    fontWeight: '300',
  },
  textInput: {
    height: 40,
    width: '100%',
    borderColor: 'gray',
    borderWidth: 1,
    padding: 10,
    marginTop: 10,
    marginBottom: 20,
  },
  imageView: {
    width: '100%',
  },
  containerProduct: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    marginBottom: 20,
    padding: 10,
  },
})
