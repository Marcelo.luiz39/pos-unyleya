import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

const { Navigator, Screen } = createStackNavigator()

import UserRegister from '../screens/UserRegister'
import HomeDetails from '../screens/HomeDetails'


export function StackRoutes() {
  return (
    <Navigator initialRouteName="Home">
      <Screen
        name="HomeDetails"
        options={{
          title: 'Lista de Usuarios',
          headerTitleAlign: 'center',
        }}
        component={HomeDetails}
      />
      <Screen
        name="Cadastrar"
        options={{
          title: 'Cadastrar Usuario',
          headerTitleAlign: 'center',
        }}
        component={UserRegister}
      />
      
    </Navigator>
  )
}