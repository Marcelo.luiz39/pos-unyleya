import { StatusBar } from 'expo-status-bar'
import { StyleSheet, Text, View } from 'react-native'

export default function App() {
  return (
    <View style={styles.container}>
      <Text style={{ color: '#fff' }}>Primeiro app no celular físico</Text>
      <Text style={{ color: '#ccc', marginTop: 10 }}>Teste de alteração</Text>
      <StatusBar style="auto" />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f83371',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
