import React, { useState, useEffect } from 'react'
import { StatusBar } from 'expo-status-bar'
import { StyleSheet, Text, SafeAreaView, View, Button } from 'react-native'

import { TextInputMask } from 'react-native-masked-text'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default function App() {
  useEffect(() => {
    AsyncStorage.getItem('@telefone')
      .then(value => setTelefone(value))
      .catch(error => alert(error))
  }, [])

  const [telefone, setTelefone] = useState('')

  const salvar = async () => {
    if (telefone == '') {
      alert('Digite um telefone válido!')
      return
    }
    try {
      await AsyncStorage.setItem('@telefone', telefone)
      alert('Telefone salvo com sucesso!')
    } catch (error) {
      alert(error)
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="auto" />

      <Text>Digite seu telefone</Text>
      {/* <TextInput
        style={styles.textInput1}
        onChangeText={text => setTelefone(text)}
        value={telefone}
      /> */}

      <TextInputMask
        style={styles.textInput1}
        type={'cel-phone'}
        options={{
          maskType: 'BRL',
          withDDD: true,
          dddMask: '(99) ',
        }}
        value={telefone}
        onChangeText={text => {
          setTelefone(text)
        }}
      />

      <View style={styles.view1}>
        <Text style={{ color: 'white' }}>Telefone: {telefone}</Text>
      </View>
      <Button
        title="Salvar"
        onPress={() => {
          salvar()
        }}
      />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center', // alinha no eixo X
    justifyContent: 'center', // alinha no eixo Y
    padding: 20,
  },

  view1: {
    width: '100%',
    marginTop: 20,
    marginBottom: 20,
    padding: 50,
    backgroundColor: 'blue',
  },

  textInput1: {
    width: '100%',
    borderColor: 'gray',
    borderWidth: 1,
  },
})
