import React from 'react'
import { View, Text, Button } from 'react-native'
import { useNavigation } from '@react-navigation/native'

export default function Home() {
  const navigation = useNavigation()

  function handleNavigateToHomeB() {
    navigation.navigate('HomeB')
  }

  return (
    <View style={{ flex: 1, backgroundColor: 'red' }}>
      <Text style={{ color: 'white' }}>Home</Text>

      <Button title="Ir para tela b" onPress={handleNavigateToHomeB} />
    </View>
  )
}
