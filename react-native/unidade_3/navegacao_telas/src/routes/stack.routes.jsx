import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

const { Navigator, Screen } = createStackNavigator()

import Home from '../screens/Home'
import HomeB from '../screens/HomeB'

export function StackRoutes() {
  return (
    <Navigator initialRouteName="Home">
      <Screen
        name="Home"
        options={{
          title: 'Tela Principal',
          headerTitleAlign: 'center',
          headerStyle: { backgroundColor: '#ccc' },
          headerTintColor: '#fff', }}
        component={Home}
      />
      <Screen
        name="HomeB"
        options={{
          headerShown: false,
        }}
        component={HomeB}
      />
    </Navigator>
  )
}
