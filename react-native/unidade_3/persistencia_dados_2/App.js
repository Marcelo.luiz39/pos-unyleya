import React, { useState, useEffect } from 'react'
import { StatusBar } from 'expo-status-bar'
import {
  StyleSheet,
  Text,
  TextInput,
  SafeAreaView,
  View,
  Button,
} from 'react-native'

import { TextInputMask } from 'react-native-masked-text'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default function App() {
  useEffect(() => {
    AsyncStorage.getItem('@data')
      .then(value => {
        if (value) {
          const user = JSON.parse(value)
          setTelefone(user.telefone)
          setName(user.name)
        }
      })
      .catch(error => alert(error))
  }, [])

  const [telefone, setTelefone] = useState('') // aqui é o estado do telefone e uma string vazia
  const [name, setName] = useState('')

  const salvar = async () => {
    const user = {
      name,
      telefone,
    }

    if (!name) {
      alert('Digite um nome válido!')
      return
    }

    if (!telefone) {
      alert('Digite um telefone válido!')
      return
    }
    try {
      //await AsyncStorage.setItem('@telefone', 12345.toString() se fosse usar um numero precisa converter para string pois so aceita string)
      await AsyncStorage.setItem('@data', JSON.stringify(user))
      // await AsyncStorage.setItem('@telefone', telefone)
      alert('Dados salvos com sucesso!')
    } catch (error) {
      alert(error)
    }
  }

  const removeItem = async () => {
    if (!telefone || !name) {
      alert('Não ha dados na memoria!')
      return
    }
    try {
      await AsyncStorage.removeItem('@data')
      alert('Dados removidos com sucesso!')
    } catch (error) {
      alert(error)
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="auto" />

      <Text>Digite seu Nome</Text>
      {/* <TextInput
        style={styles.textInput1}
        onChangeText={text => setTelefone(text)}
        value={telefone}
      /> */}

      <Text>Digite Seu Telefone</Text>
      <TextInput
        style={styles.textInput1}
        value={name}
        onChangeText={value => setName(value)}
      />

      <TextInputMask
        style={styles.textInput1}
        type={'cel-phone'}
        options={{
          maskType: 'BRL',
          withDDD: true,
          dddMask: '(99) ',
        }}
        value={telefone}
        onChangeText={text => {
          setTelefone(text)
        }}
      />

      <View style={styles.view1}>
        <Text style={{ color: 'white' }}>Telefone: {telefone}</Text>
      </View>
      <Button
        title="Salvar"
        onPress={() => {
          salvar()
        }}
      />
      <Button
        title="Limpar"
        onPress={() => {
          removeItem()
        }}
      />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center', // alinha no eixo X
    justifyContent: 'center', // alinha no eixo Y
    padding: 20,
  },

  view1: {
    width: '100%',
    marginTop: 20,
    marginBottom: 20,
    padding: 50,
    backgroundColor: 'blue',
  },

  textInput1: {
    width: '100%',
    borderColor: 'gray',
    borderWidth: 1,
  },
})
