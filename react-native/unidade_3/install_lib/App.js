import React, { useState } from 'react'
import { StatusBar } from 'expo-status-bar'
import { StyleSheet, Text, SafeAreaView, View } from 'react-native'

import { TextInputMask } from 'react-native-masked-text'

export default function App() {
  const [telefone, setTelefone] = useState('')

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="auto" />

      <Text>Digite seu telefone</Text>
      {/* <TextInput
        style={styles.textInput1}
        onChangeText={text => setTelefone(text)}
        value={telefone}
      /> */}

      <TextInputMask
        style={styles.textInput1}
        type={'cel-phone'}
        options={{
          maskType: 'BRL',
          withDDD: true,
          dddMask: '(99) ',
        }}
        value={telefone}
        onChangeText={text => {
          setTelefone(text)
        }}
      />

      <View style={styles.view1}>
        <Text style={{color: 'white'}}>Telefone: {telefone}</Text>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center', // alinha no eixo X
    justifyContent: 'center', // alinha no eixo Y
    padding: 20,
  },
  view1: {
    width: '100%',
    marginTop: 20,
    padding: 50,
    backgroundColor: 'blue',
  },
  textInput1: {
    width: '100%',
    borderColor: 'gray',
    borderWidth: 1,
  },
})
