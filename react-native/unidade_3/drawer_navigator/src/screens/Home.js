import React from 'react'
import { View, Text, Button, StyleSheet } from 'react-native'
import { useNavigation } from '@react-navigation/native'

export default function Home() {
  const navigation = useNavigation()

  function handleNavigateToHomeB() {
    navigation.navigate('HomeB')
  }

  return (
    <View style={styles.container}>
      <Text style={styles.text1}>Tela Principal</Text>

      <Button title="Tela 2" onPress={handleNavigateToHomeB} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ccc',
    justifyContent: 'center',
  },
  text1: {
    textAlign: 'center',
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
  },
})
