import React from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'
import { useNavigation } from '@react-navigation/native'

export default function HomeB() {
  const navigation = useNavigation()

  function handleNavigateToHome() {
    navigation.navigate('Home')
  }

  return (
    <View style={styles.container}>
      <Text style={styles.text1}>Tela 2</Text>

      <Button title="Tela Home" onPress={handleNavigateToHome} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ccc',
    justifyContent: 'center',
  },
  text1: {
    textAlign: 'center',
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
  },
})
