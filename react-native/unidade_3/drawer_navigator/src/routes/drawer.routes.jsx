import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { MaterialIcons } from '@expo/vector-icons'

const { Navigator, Screen } = createDrawerNavigator()

import Home from '../screens/Home'
import HomeB from '../screens/HomeB'

export function DrawerRoutes() {
  return (
    <Navigator initialRouteName="Home">
      <Screen name="Home"
      options={{
        drawerLabel: 'Home',
        drawerIcon: () => <MaterialIcons name="home" size={22} />,
      }} 
      
      component={Home} />

      <Screen
        name="HomeB"
        options={{
          drawerLabel: 'Sair',
          drawerIcon: () => <MaterialIcons name="logout" size={22} />,
        }}
        component={HomeB}
      />
    </Navigator>
  )
}
