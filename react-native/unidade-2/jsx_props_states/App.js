import React, { useState } from 'react'
import { Text, View, Button } from 'react-native'

import { Image } from 'react-native'

const Bananas = () => {
  const pic = {
    uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg',
  }
  return <Image source={pic} style={{ width: 193, height: 110 }} /> //propriedade source recebe o objeto pic style é um objeto com as propriedades width e height
}

export default function App() {
  const [count, setCount] = useState(0)

  return (
    <View>
      <Text>Imagem de Banana</Text>
      <Text>Você clicou {count} vezes</Text>
      <Bananas />
      <Button
        onPress={() => setCount(count + 1)}
        title="Clique aqui"
        color="#841584"
        
      />
    </View>
  )
}
