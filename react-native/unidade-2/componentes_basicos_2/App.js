import React, { useState } from 'react'
import {
  Text,
  ScrollView,
  Switch,
  ActivityIndicator,
  Modal,
  Button,
  View,
} from 'react-native'

export default function App() {
  const [valueSwitch, setValueSwitch] = useState(false)
  const [visible, setVisible] = useState(true)

  const toggleSwitch = () => {
    setValueSwitch(true)
  }

  const toggleModal = () => {
    setVisible(true)
  }
  return (
    <ScrollView>
      <Button title="Open Modal" onPress={toggleModal} />

      <Modal visible={visible}>
        <View style={{ width: 200, height: 200, backgroundColor: 'blue' }}>
        <Text>Switch Example</Text>
        <Switch
          trackColor={{ false: '#767577', true: '#81b0ff' }}
          thumbColor={valueSwitch ? '#f5dd4b' : '#f4f3f4'}
          ios_backgroundColor="#3e3e3e"
          onValueChange={toggleSwitch}
          value={valueSwitch}
        />
        </View>

        {valueSwitch ? (
          <Text>Switch is ON</Text>
        ) : (
          <ActivityIndicator size="large" color="#0000ff" />
        )}
      </Modal>
    </ScrollView>
  )
}
