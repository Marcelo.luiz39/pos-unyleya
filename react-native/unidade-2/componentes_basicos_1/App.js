import React, { useState } from 'react'
import {
  View,
  Text,
  TextInput,
  Button,
  TouchableOpacity,
  Image,
  ImageBackground,
} from 'react-native'

export default function App() {
  const [nome, setNome] = useState('')

  const addNome = () => {
    alert(`Olá ${nome}`)
  }

  return (
    <ImageBackground
      source={{
        uri: 'https://images.pexels.com/photos/2088207/pexels-photo-2088207.jpeg?auto=compress&cs=tinysrgb&w=400',
      }}
      style={{ width: '100%', height: '100%' }}
    >
      <Text
        style={{
          color: 'red',
          fontSize: 25,
          alignSelf: 'center',
          marginTop: 100,
        }}
      >
        Olá Mundo!
      </Text>

      <Button title="Clique aqui" onPress={() => alert('Clicou no 1 botão')} />

      <Image
        source={require('./assets/cachorro.jpg')}
        style={{ width: 200, height: 200, alignSelf: 'center', marginTop: 50 }}
      />

      <TextInput
        style={{
          backgroundColor: '#ccc',
          height: 45,
          width: 200,
          alignSelf: 'center',
          marginTop: 50,
          borderRadius: 10,
          paddingLeft: 10,
          color: '#fff',
        }}                       
        placeholder="Digite seu nome"
        onChangeText={text => setNome(text)}
      />

      <TouchableOpacity
        style={{
          backgroundColor: 'green',
          height: 55,
          width: 200,
          alignSelf: 'center',
          marginTop: 50,
          borderRadius: 10,
        }}
        onPress={() => addNome()}
      >
        <Text
          style={{
            color: '#fff',
            fontSize: 20,
            fontWeight: 'bold',
            alignSelf: 'center',
            marginTop: 10,
          }}
        >
          Pressione aqui
        </Text>
      </TouchableOpacity>
    </ImageBackground>
  )
}
