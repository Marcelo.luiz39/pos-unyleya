import { StatusBar } from 'expo-status-bar'
import { View, Text, StyleSheet, ScrollView } from 'react-native'

export default function App() {
  return (
    <ScrollView>
      <View
        style={{
          backgroundColor: 'yellow',
          marginTop: 50,
          flexDirection: 'row',
          flexWrap: 'wrap',
        }}
      >
        <StatusBar style="dark" />
        <View style={styles.viewVerde} />
        <View style={styles.viewAzul} />
        <View style={styles.viewVermelha} />
      </View>

      <View
        style={{
          backgroundColor: 'yellow',
          marginTop: 50,
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',
        }}
      >
        <View style={styles.viewVerde1} />
        <View style={styles.viewAzul1} />
        <View style={styles.viewVermelha1} />
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  viewVerde: {
    backgroundColor: 'green',
    height: 150,
    width: 150,
  },
  viewAzul: {
    backgroundColor: 'blue',
    height: 150,
    width: 150,
  },
  viewVermelha: {
    backgroundColor: 'red',
    height: 150,
    width: 150,
  },

  viewVerde1: {
    backgroundColor: 'green',
    height: 50,
    width: 50,
  },
  viewAzul1: {
    backgroundColor: 'blue',
    height: 50,
    width: 50,
  },
  viewVermelha1: {
    backgroundColor: 'red',
    height: 50,
    width: 50,
  },
})
