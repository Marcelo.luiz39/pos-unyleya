import {
  StatusBar,
  ImageBackground,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native'
import React, { useState } from 'react'

const App = () => {
  const [gasolina, setGasolina] = useState('')
  const [alcool, setAlcool] = useState('')
  const [resultado, setResultado] = useState('')

  const calcular = () => {
    const tempGasolina = parseFloat(gasolina)
    const tempAlcool = parseFloat(alcool)
    //const resultado = tempGasolina * 0.7 > tempAlcool

    setResultado(tempGasolina * 0.7 > tempAlcool ? 'Alcool' : 'Gasolina')

    // if (resultado) {
    //   setResultado('Alcool')
    //   return
    // }
    // setResultado('Gasolina')

    // setResultado(tempGasolina * 0.7 > tempAlcool ? 'Alcool' : 'Gasolina')

    // if (tempGasolina * 0.7 > tempAlcool) {
    //   setResultado('Alcool')
    // } else {
    //   setResultado('Gasolina')
    // }

    setGasolina('')
    setAlcool('')
  }

  return (
    <ImageBackground
      source={{
        uri: 'https://img.freepik.com/fotos-premium/feche-o-carro-no-posto-de-gasolina-sendo-abastecido_293953-156.jpg?size=626&ext=jpg',
      }}
      style={styles.container}
    >
      <StatusBar hidden={true} />
      <Text style={styles.textTitle}>Álcool ou Gasolina</Text>
      <Text style={styles.textOption}>
        Escolha a melhor opção para bastecer:
      </Text>

      <TextInput
        placeholder="Preço da Gasolina"
        style={styles.textInput}
        keyboardType="numeric"
        value={gasolina}
        onChangeText={setGasolina}
      />

      <TextInput
        placeholder="Preço do Álcool"
        style={styles.textInput}
        keyboardType="numeric"
        value={alcool}
        onChangeText={setAlcool}
      />

      <TouchableOpacity style={styles.bottom} onPress={calcular}>
        <Text style={{ color: 'white', fontSize: 16, fontWeight: 'bold' }}>
          Calcular
        </Text>
      </TouchableOpacity>

      <Text style={styles.bestOption}>Melhor opção:</Text>
      <Text style={styles.chosenAmount}>{resultado}</Text>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  textTitle: {
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: 50,
  },

  textOption: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 30,
  },

  bestOption: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 30,
  },

  chosenAmount: {
    fontSize: 32,
    color: 'white',
    fontWeight: 'bold',
    marginTop: 10,
  },

  textInput: {
    width: '90%',
    height: 50,
    padding: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    marginTop: 20,
    fontSize: 16,
  },

  bottom: {
    width: '90%',
    height: 50,
    backgroundColor: '#0c9ad5',
    marginTop: 30,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default App
