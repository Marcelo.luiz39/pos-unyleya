import React, { useState } from 'react'
import {
  StatusBar,
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  Switch,
  TouchableOpacity,
  TextInput,
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

export default function App() {
  const [saldo, setSaldo] = useState(0)
  const [valor, setValor] = useState('')
  const [isSwitchEnabled, setIsSwitchEnabled] = useState(false)

  const adicionar = () => {
    if (valor === '') {
      alert('Digite um valor')
      return
    }
    const temp = parseFloat(valor)
    setSaldo(saldo + temp)
    setValor('')
  }

  const retirar = () => {
    if (valor === '') {
      alert('Digite um valor')
      return
    } else if (saldo < valor) {
      alert('Saldo insuficiente')
      setValor('')
      return
    }
    const temp = parseFloat(valor)
    setSaldo(saldo - temp)
    setValor('')
  }

  const toggleSwitch = () => setIsSwitchEnabled(previousState => !previousState)

  return (
    <SafeAreaView style={{ padding: 16 }}>
      <Text style={styles.textCard}>Carteira Digital</Text>

      <View style={styles.view1}>
        <View style={styles.container}>
          <Text style={styles.textBtn}>Seu Saldo</Text>
          <Switch
            trackColor={{ false: '#767577', true: '#dcd' }}
            ios_backgroundColor="#3e3e3e"
            value={isSwitchEnabled}
            onValueChange={toggleSwitch}
          />
        </View>

        {isSwitchEnabled ? (
          <Text style={styles.saldo}>R$ {saldo.toFixed(2)}</Text>
        ) : (
          <Text style={styles.saldo}>
            R$ {''}
            <Icon name="eye-slash" style={styles.eye} />
          </Text>
        )}

        <View style={styles.input}>
          <TextInput
            style={styles.input}
            placeholder="Digite o valor"
            underlineColorAndroid="transparent"
            placeholderTextColor={'#7b8794'}
            keyboardType="numeric"
            onChangeText={value => setValor(value)}
            value={valor}
          />
          <Icon name="money" style={styles.icon} />
        </View>

        <View style={styles.view2}>
          <TouchableOpacity style={styles.button} onPress={() => adicionar()}>
            <Text style={styles.textBtn}>Adicionar</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button} onPress={() => retirar()}>
            <Text style={styles.textBtn}>Retirar</Text>
          </TouchableOpacity>
        </View>
      </View>

      <StatusBar style="auto" />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  textCard: {
    marginBottom: 20,
    fontWeight: 'bold',
    fontSize: 18,
  },

  saldo: {
    fontSize: 30,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
    marginTop: 20,
  },

  icon: {
    fontSize: 30,
    color: '#7b8794',
    position: 'absolute',
    top: 22,
    right: 35,
  },

  eye: {
    fontSize: 30,
    color: '#fff',
  },

  input: {
    flex: 1,
    flexDirection: 'row',
    height: 50,
    paddingLeft: 6,
    marginHorizontal: 20,
    borderRadius: 8,
    marginVertical: 10,
    marginBottom: 50,
    backgroundColor: '#F8F9FA',
  },

  view1: {
    backgroundColor: '#823bd1',
    borderRadius: 10,
    padding: 16,
  },

  view2: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },

  button: {
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    padding: 10,
    marginVertical: 20,
    height: 50,
  },

  textBtn: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
})
