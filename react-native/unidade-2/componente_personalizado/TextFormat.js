import React from 'react'
import { Text, View, Image } from 'react-native'

export default function Component() {
  return (
    <View style={{ backgroundColor: 'blue' }}>
      <Text
        style={{
          color: '#fff',
          fontSize: 20,
          fontWeight: 'bold',
          marginTop: 10,
          padding: 10,
          textAlign: 'center',
        }}
      >
        Componente Personalizado!
      </Text>
      <Image source={require('./abstract.jpeg')} />
    </View>
  )
}
