import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import TextFormat from './TextFormat'

export default function App() {
  return (
    <View>
      <Text style={{marginTop: 60 }}>Hello World!</Text>
      <TextFormat />
    </View>
  )
}
