import { StatusBar } from 'expo-status-bar'
import { View, StyleSheet, FlatList, Text } from 'react-native'

export default function App() {
  const dados = [
    { id: '1', name: 'Julia', age: 19 },
    { id: '2', name: 'Kaique', age: 21 },
    { id: '3', name: 'Marcelo', age: 43 },
  ]
  return (
    <View style={styles.container}>
      <StatusBar style="dark" />

      <FlatList
        data={dados}
        renderItem={({ item }) => (
          <View style={styles.viewFlat}>
            <Text style={styles.text}>Nome: {item.name}</Text>
            <Text style={styles.text}>Idade: {item.age}</Text>
            
          </View>
        )}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#5f41e5',
    flex: 1,
    marginTop: 60,
    paddingTop: 40,
    paddingHorizontal: 20,
  },
  viewFlat: {
   
    backgroundColor: '#fff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    marginBottom: 5,
    borderRadius: 10,
  },
  text: {
    color: '#E64589',
    fontSize: 20,
    fontWeight: 'bold',
    fontStyle: 'italic',
    marginBottom: 5,
    marginTop: 5,
  },
})
