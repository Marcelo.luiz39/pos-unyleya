import React, { useEffect, useState } from 'react'
import { View, Text, Button } from 'react-native'

export default function App() {
  const [valor, setValor] = useState(0)

  const aumentar = () => setValor(() => valor + 1)

  useEffect(() => {
    alert('O valor foi alterado')
  }, [])

  return (
    <View>
      <Text style={{ fontSize: 50 }}>{valor}</Text>
      <Button title="Aumentar" onPress={aumentar} />
    </View>
  )
}
