import { StatusBar } from 'expo-status-bar'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

export default function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Hello World!</Text>
      <StatusBar style="auto" />

      <View>
        <TouchableOpacity style={styles.touch} onPress={() => alert('Bem vindo!')}>
          <Text style={styles.textTouch}>Click Me</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    marginTop: 50,
    height: 150,
    width: 150,
  },
  text: {
    alignSelf: 'center',
    marginTop: 50,
    color: '#ccc',
    fontSize: 22,
    fontWeight: 'bold',
  },
  touch:{
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
    marginTop: 20,
  },
  textTouch:{
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold',
    alignSelf: 'center',
  }
})
