import React from 'react'
import { Text, View, Image } from 'react-native'

export default function Component({ texto = 'Sem mensagens', idade = '0' }) {
  return (
    <View style={{ backgroundColor: 'blue', marginBottom: 10, padding: 10 }}>
      <Text
        style={{
          color: '#fff',
          fontSize: 20,
          fontWeight: 'bold',
          marginTop: 10,
          padding: 10,
          textAlign: 'center',
        }}
      >
        {texto}
      </Text>
      <Text
        style={{
          color: '#fff',
          fontSize: 20,
          fontWeight: 'bold',
          alignSelf: 'center',
        }}
      >
        {' '}
        idade: {idade}
      </Text>
      <Image
        source={require('./abstract.jpeg')}
        style={{ width: 100, height: 100 }}
      />
    </View>
  )
}
