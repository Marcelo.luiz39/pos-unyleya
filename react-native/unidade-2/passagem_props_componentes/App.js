import React from 'react'
import { ScrollView, Text } from 'react-native'

import TextFormat from './TextFormat'

export default function App() {
  return (
    <ScrollView>
      <Text style={{marginTop: 60 }}>Hello World!</Text>
      <TextFormat texto='ola'/>
     <TextFormat texto='Feliz ano novo'/>
      <TextFormat idade={43} texto='Marcelo'/>
      <TextFormat texto='seja bem vindo ao curso'/>
       <TextFormat texto='ate mais'/>
       <TextFormat />
    </ScrollView>
  )
}
