// Enumeradores

// Todos os enumeradores ( Tipos ) começa com a letra maiuscula

enum Compass {
    case north, south, east, west
}

//var direction = Compass.west  esta forma é a normal porem podemos usar a forma resumida
var direction: Compass = .east

switch direction {
case .east:
    print("Indo para o Leste")
case .north:
    print("Indo para o Norte")
case .south:
    print("Indo para o Sul")
case .west:
    print("Indo para o Oeste")
}

print("")

enum Planet {
    case mercury, venus, earth, jupiter, mars
}

// Enum com valor padrão
enum WeekDay: String {
    case sunday = "Domingo"
    case monday = "Segunda"
    case tuesday
    case wednesday
    case thursday
    case friday
    case saturday
}

var weekDay: WeekDay = .sunday
print(weekDay.rawValue)  // a propriedade recupera o valor padrão

print("")

let monday = WeekDay(rawValue: "Segunda")
if let monday {
    print(monday.rawValue)
}

print("")

// Valor associado (Associated Value)
enum Measure {
    case age(Int)
    case weight(Double)
    case size(width: Double, height: Double)
}

let measure: Measure = .weight(120.0)

switch measure {
case .age(let age):
    print("A medida é uma idade: A idade é \(age)")
case .weight(let weight):
    print("A medida é um peso: O peso é \(weight)")
case .size(let width, let height):
    print("Ä medida é um tamanho e uma altura: A altura é \(width) A largura é \(height)")
}
