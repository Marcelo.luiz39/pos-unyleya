var number = 44

number += 2

number -= 10

number *= 2

number /= 2

number + 1

number - 1

number * 2

number / 2

var isHungry = false
var isUgly = true

var isOgre = isHungry && isUgly  // Aqui para dar true precisa que os dois sejam true e para dar false apenas um

isOgre = isUgly || isHungry  // aqui precisa que os dois sejam false para dar false e apenas um true para dar true

isOgre  = !isOgre  // Aqui está negando o resultado

var grade = 7.5
var finalResult = grade > 7.0 ? "Aprovado": "Reprovado"

// Aqui vai imprimir de 1 ate 10

for number in 1...100 {
    print(number)
}

print("")  // Aqui estou pulando uma linha

// Aqui vai imprimir de 1 ate 9

for number2 in 1..<10 {
    print(number2)
}

var numbers = Int("aaa") ?? 0  // Coalescing Operator
