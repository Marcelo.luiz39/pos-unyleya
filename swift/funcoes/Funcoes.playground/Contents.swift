// Funções

// Função sem retorno
func hello() {
    print("Hello World!")
}

hello()

// Funçao com retorno
func getPiNumber() -> Double {
    return 3.14
}

let pi = getPiNumber()
print(pi)


// Funções com paramentro
func calculateTax(value: Double, percent: Double) -> Double {
    return value * percent / 100
}

let tax = calculateTax(value: 120, percent: 2.5)
print(tax)

/*
 func say(sentence: String, person: String) {
   print("\(sentence) \(person)")
}

say(sentence: "Ola", person: "Marcelo Luiz")
 */


/*
 func say(greeting sentence: String, to person: String) {
 print("\(sentence) \(person)")
 }
 
 say(greeting: "Ola", to: "Marcelo Luiz")
 */

func say(_ sentence: String, to person: String) {
    print("\(sentence) \(person)")
}

say("Ola", to: "Marcelo Luiz")

func sum(_ initialNumber: Int, withNumbers numbers: Int...) -> Int {
    var result = initialNumber
    
    for number in numbers {
        result += number
    }
    
    return result
}

print(sum(10, withNumbers: 1,2,3,4,6,9,18))  // Aqui o withNumbers representa vários parâmetros usando a conotação de 3 pontos ( ... )


// Usando função no parâmetro
func sum(a: Double, b: Double) -> Double {
    return a + b
}
func subtract(a: Double, b: Double) -> Double {
    return a - b
}
func multiply(a: Double, b: Double) -> Double {
    return a * b
}
func divide(a: Double, b: Double) -> Double {
    return a / b
}

// Para saber o tipo da func retiramos o nome, o retorno e o tipo " (Double, Double) -> Double ) "

func aply(a: Double, b: Double, operation: (Double, Double) -> Double ) -> Double {
    return operation(a, b)
}

aply(a: 10, b: 20, operation: multiply)

// Funções que retorna outras funções

typealias Operation = (Double, Double) -> Double  // Aqui eu dei um nome para o retorno da função ( o nome sempre em maiuscula)

func getOperation(named operation: String) ->  Operation{
    switch operation {
    case "soma":
        return sum
    case "subtração":
        return subtract
    case "multiplicação":
        return multiply
    default:
       return divide
    }
}

let operation = getOperation(named: "soma")
operation(30, 10)
