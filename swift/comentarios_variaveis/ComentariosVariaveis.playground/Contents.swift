import UIKit

//var greeting = "Hello, playground"

var life: Int = 9
var power: Int = 100
var enemyPower: Int = 150

func fight() {
    /*
    if power < enemyPower {
        life -= 1
    }
    print("Inimigos lutando")
     */
}

fight()

// Com tipo implicito

var name: String = "Marcelo Luiz"
var fullName: String = "Marcelo Luiz Souza" // Aqui usa-se camelcase
var age: Int = 44
var sallary: Double = 2300.50
age -= 10
sallary += 2300

// Com inferencia de tipo

let cpf = "012.345.678-90"
//cpf = "123.456.789-00"  aqui da erro pois let é uma constante

// age = 145.7 fortemente tipada não podemos inferir outro tipo
