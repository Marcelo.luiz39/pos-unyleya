// Structs

/*
struct Person {
    var name: String
    var age: Int
    
    func run() {
        print("\(name) Está correndo")
    }
}

var marcelo = Person(name: "Marcelo", age: 44)  // Aqui como é uma struct ele já me entrega o construtor padrão
print(marcelo)
*/

struct Person {
    var name: String
    var age: Int = 0
    
    func run() {
        print("\(name) Está correndo")
    }
    
/*
 init(name: String) {
        self.name = name
         self.age = 0 alimento aqui ou diretamente na propriedade
    }

    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
 */
    
    mutating func changeAge(newAge: Int) {
        self.age = newAge
    }
}  // para mudar uma propriedade da struct usamos mutating
 



var marcelo = Person(name: "Marcelo", age: 44)
print(marcelo)
marcelo.run()
marcelo.changeAge(newAge: 54)
marcelo.age

// Nas struct os valores são passados por copia e não por referência exemplo:
var julia = marcelo

print("\(marcelo.name) tem \(marcelo.age) anos e \(julia.name) tem \(julia.age) anos")

julia.name = "Julia"
julia.age = 19

print("\(marcelo.name) tem \(marcelo.age) anos e \(julia.name) tem \(julia.age) anos")


