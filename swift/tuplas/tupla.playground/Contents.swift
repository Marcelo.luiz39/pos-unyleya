import UIKit

/*
 Tupla
 
 É um tipo que pode conter outros tipos, inclusive outra tupla
 Seus elementos podem ser nomeados
 Pode ser decomposta em variaveis e usamos _ quando não precisamos de algum elemento
 
 */

// Tupla com elementos nomeados
var exam: (stundent: String, year: Int, discipline: String, grade: Double) = ("Marcelo Souza", 2, "IOS", 8.5)

// Acessando pelo indice do elemento (lembrando que começa em 0 como em um array)
print("A nota do aluno foi \(exam.3)")

// Acessando pelo nome do elemento
print("\(exam.stundent), do \(exam.year) ano, tirou nota \(exam.grade) na materia \(exam.discipline)")

// Decompondo uma tupla em variaveis
// Senão desejamos algum elemento, usamos _
let (student, _, discipline, _) = exam
print("\(student) fez a prova de \(discipline)")
