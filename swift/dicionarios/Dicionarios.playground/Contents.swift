// Dicionarios
// Dicionarios sempre retorna optional

var emptyDic: [String: Int] = [:]  // Dic vazio
print(emptyDic)

print("")

var states: [String: String] = [
    "BA": "Bahia",
    "SP": "São Paulo",
    "RJ": "Rio de Janeiro",
    "MG": "Minas Gerais",
    "PA": "Pará"
]
print(states)

print("")

print(states.isEmpty)

print("")

print(states.count)

print("")

if let rj = states["RJ"] {
    print("RJ é \(rj)")
} else {
    print("Este estado não existe")
} // desembrulhando o optional e verificando se existe

print("")

if states["AA"] == nil {
    print("Não tem esté estado")
}

print("")

states["GO"] = "Goias"  // adicionando
print(states)

states["PA"] = nil  // removendo de forma simples
states

print("")

states.removeValue(forKey: "GO")  // removendo pela chave
print(states)

print("")

for state in states {
    print(state)
}  // aqui retorna uma tupla nomeada

print("")

for state in states {
    print(state.self)
}  // Aqui retorna a mesma coisa da variavel

print("")

for state in states {
    print(state.key)
}

print("")

for state in states {
    print(state.value)
}

print("")

for (uf, state) in states {
    print("A sigla do estado é \(uf) o nome da cidade é \(state)")
}

print("")

for uf in states.keys {
    print(uf)
}

print("")

for name in states.values {
    print(name)
}

print("")

print(states)

print("")

for state in states {
    print(state)
}
