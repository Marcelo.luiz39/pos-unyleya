// Closurers Reduzidas

//{(a: Double, b: Double) -> Double in
//    return a/b
//}

typealias Func = (Int, Int) -> Int

func calculate(a: Int, b: Int, operation: Func) -> Int {
    return operation(a, b)
}

calculate(a: 8, b: 3, operation: {(x: Int, y: Int) -> Int in
    return x % y
})

calculate(a: 8, b: 3, operation: {(x, y) -> Int in
    return x % y
})

calculate(a: 8, b: 3, operation: {x, y -> Int in
    return x % y
})

calculate(a: 8, b: 3, operation: {x, y in
    return x % y
})

calculate(a: 8, b: 3, operation: {
    return $0 % $1
})

calculate(a: 8, b: 3, operation: {$0 % $1})

calculate(a: 8, b: 3){$0 % $1}


var names = ["Marcelo", "Julia", "Kaique", "Alex", "Marilene"]

var names2: [String] = []
for name in names {
    names2.append(name.uppercased())
}
print(names2)

// Closures in methods
var names3 = names.map({$0.uppercased()})  // Aqui usamos clousers de apenas um parametro com ($0) e transformei em uppercase
print(names3)
