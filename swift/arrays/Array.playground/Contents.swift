// Arays

var emptyArray: [Int] = [] // Quando vamos iniciar sem valores precisamos declarar o tipo
var games = ["Sonic", "Good of War", "Forza"]

games.count
games.isEmpty

let firstGame = games[0]
let lastGame = games[2]
games[1] = "Strider"
games

games.append("Returnal")
games.insert("Gears os War", at: 2)

let gears = games.remove(at: 2)
games

if games.contains("Mario"){
    print("Temo jogo Mario bros na lista")
} else {
    print("Não tem o jogo Mario na lista")
}

for game in games {
    print("Temos o jogo \(game) na lista")
}

if let index = games.firstIndex(of: "Mario") {
    print("Forza encontra-se no indice \(index)")
} else {
    print("Não temos este jogo na lista")
}

// Aqui caso nao queiramos utilizar uma variavel ( usamos undeline )
if let _ = games.firstIndex(of: "Mario") {
    print("Forza encontra-se no meu array")
} else {
    print("Não temos forza na lista")
}
