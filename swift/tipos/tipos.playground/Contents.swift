import UIKit

/*
 
 Int, UInt, Double, Float, String, Character, Bool
 
 Variações de Int(signed): Int8, Int16, Int32, Int64
 Versões sem sinal de Int(valores positivos)usinged: Uint8, UInt16, UInt33, UInt64
 
 */

// String Character
var title: String = "aluno"
var name = "Marcelo Luiz"
var gender: Character = "M" // aqui precisamos definir o tipo explicitamente ou o compilador ira dedinir como String
var isMale: Bool = true

// Int
var number = 12 // Int por inferência
var studentAge: UInt8 = 44

// Double Float
var sallary: Double = 1500.00
var grade = 7.5 // Double por inferência
var pi: Float = 3.14 // aqui precisamos definir o tipo explicitamente ou o compilador ira dedinir como Double

// Interpolação de String
var finalResult = "O \(title) \(name) tirou nota \(grade)"

print(finalResult)

/*
 let dog1 = "Rex"
 let dog2 = "Tabata"
 let poo = "cocô"
 
 print("\(dog1) e \(dog2) adoram fazer \(poo)")
 */

let 🐕 = "Rex"
let 🐩 = "Tabata"
let 💩 = "cocô"

print("\(🐕) e \(🐩) adoram fazer \(💩)")
