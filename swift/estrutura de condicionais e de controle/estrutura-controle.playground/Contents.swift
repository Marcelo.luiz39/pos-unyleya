// if -> else -> else if

let temperature = 14.1

if temperature > 30.0 {
    print("Está Quente")
} else if temperature > 20.0 {
    print("Está agradavel")
} else if temperature > 14.0 {
    print("Está Frio")
} else {
    print("Está muito frio")
}

// switch
// Precisa ser exaurido, ou seja ter todos os cenarios cobertos e ter o default sempre.

let letter: Character = "b"
switch letter {
case "a":
    print("Está é a primeira letra do alfabeto")
case "z":
    print("Está é a ultima letra do alfabeto")
default:
    print("Outra letra do alfabeto")
}

// while
var money: Double = 100.0
while money > 0 {
    money -= 10.0
    print("Agora você tem R$ \(money)")
}

//repeat while
var luckNumber: Int = 0
var number: Int = 0
var tries: Int = 0
repeat {
    number = Int.random(in: 0...20)
    tries += 1
} while number != luckNumber
print("Você acertou o número da sorte em \(tries) tentativa(s)")

// for in

print("Veja a tabuada do 7")
for number in 1...10 {
    print("7 x \(number) = \(number * 7)")
}

print("Os multiplos de 3 começando em 0 até 100 são:")
for number in stride(from: 0, through: 100, by: 3) {
    print(number)
}
