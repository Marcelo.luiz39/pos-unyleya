import UIKit

// Optional
var driveLicense: String = ""// aqui é uma variavel sem valor e nao uma variavel vazia

var driveLicense2: String?
// var driveLicense2: String = nil aqui é a mesma coisa que o codigo acima

print(driveLicense2)
driveLicense2 = "84564563"

print(driveLicense2)


// Optional Binding
if let driveLicenseValue = driveLicense2 {
    print(driveLicenseValue)
} else {
    print("O usuario não tem carteira de motorista!")
}


// Optional Binding NÃO SEGURO usamos o sinal de exclamção ! porem se o valor não existir vai dar crash no APP

var cpf: String?

cpf = "123.456.789-00"

//print(cpf!)

/* Implicity Unwrapped Optional */

var cpf2: String!  // aqui outra forma de optional

cpf2 = "009.876.543-21"

print(cpf2)

var number: Double!

number = 76.8

print("O valor é \(number * 2)")

/* Operador de coalescência nula */
let myCpf: String = cpf ?? "000.000.000-00"

var myNumber = Int("23a")

print(myNumber)

var myNumber2: Int = Int("23a") ?? 0

print(myNumber2)



