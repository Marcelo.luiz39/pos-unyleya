// Herança e polimorfismo

class vehicle {
    
    //Propriedade armazenada
    private var internalSpeed: UInt = 0
    var model: String
    
    init(model: String) {
        self.model = model
    }
    
    
    // Propriedade computada
    var maxSpeed: UInt {
        400
    }
    
    var currentSpeed: String {
    //    return "\(speed) km/h"
        return "\(speed) \(vehicle.speedUnit)"
    }
    
    var speed: UInt {
        get {
           return internalSpeed
        }
        set {
            internalSpeed = min(newValue, maxSpeed)
        }
    }
    
    //Propriedade estatica
    static var speedUnit = "km/h"
    
    // Método estatico (método de classe)
    class func alert() -> String {
        return "Se beber não diriga"
    }
    
    func description() {
        print("Veiculo: \(model), velocidade: \(internalSpeed)")
    }
}

var ferrari = vehicle(model: "Ferrari")
ferrari.speed = 520
print(ferrari.model, ferrari.speed)
vehicle.speedUnit = "Milhas/hora"
vehicle.speedUnit = "km/h"
vehicle.alert()



print(ferrari.currentSpeed)
ferrari.description()
print("A \(ferrari.model) está a \(ferrari.currentSpeed) \(vehicle.alert())")


class Car: vehicle {
    var driver: String
    
    override var maxSpeed: UInt {
        250
    }
    
    init(driver: String, model: String) {
        self.driver = driver
        super.init(model: model)
    }
    
    override func description() {
        print("Veiculo: \(model), Velocidade: \(currentSpeed) Motorista: \(driver)")
    }
}

let fusca = Car(driver: "Marcelo", model: "Fusca")
print("O condutor \(fusca.driver) é dono do veiculo \(ferrari.model) e está a \(ferrari.currentSpeed)")
fusca.speed = 300
print(fusca.currentSpeed)
print(fusca.description())
print(ferrari.description())
