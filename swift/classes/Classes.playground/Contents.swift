// Classes

class Person {
    var name: String
    var age: Int
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
    
    func changeAge(newAge: Int) {
        self.age = newAge
    }
}

var marcelo = Person(name: "Marcelo", age: 44)
marcelo.changeAge(newAge: 34)

var julia = marcelo
marcelo.age = 44

print(marcelo.age, julia.age)

julia.age = 19

print(marcelo.age, julia.age)


class vehicle {
    
    //Propriedade armazenada
    private var internalSpeed: UInt = 0
    var model: String
    
    init(model: String) {
        self.model = model
    }  // Aqui não pediu a propriedade speed pois já esta como valor default
    
    
    // Propriedade computada
    var currentSpeed: String {
    //    return "\(speed) km/h"
        return "\(speed) \(vehicle.speedUnit)"
    }
    
    var speed: UInt {
        get {
           return internalSpeed
        }
        set {
            internalSpeed = min(newValue, 220)
        }
    }
    
    //Pripriedade estatica
    // sem o " STATIC " a variavel é da propriedade ferrari com o " STATIC " a propriedade e da classe Vehicle
    static var speedUnit = "km/h"
    
    // Método estatico (método de classe)
    // Mesma coisa do static ou seja sem o " CLASS " ele e método da ferrari com o " CLASS " e metodo de Vehicle
    class func alert() -> String {
        return "Se beber não diriga"
    }
}

var ferrari = vehicle(model: "Ferrari")
ferrari.speed = 520
print(ferrari.model, ferrari.speed)
vehicle.speedUnit = "Milhas/hora"
vehicle.speedUnit = "km/h"
vehicle.alert()



print(ferrari.currentSpeed)
print("A \(ferrari.model) está a \(ferrari.currentSpeed) \(vehicle.alert())")
