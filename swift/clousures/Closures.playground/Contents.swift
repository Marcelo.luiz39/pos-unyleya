// Closurers

//{(a: Double, b: Double) -> Double in
//    return a/b
//}

typealias Func = (Int, Int) -> Int

func calculate(a: Int, b: Int, operation: Func) -> Int {
    return operation(a, b)
}

calculate(a: 8, b: 3, operation: {(x: Int, y: Int) -> Int in
    return x % y
})

calculate(a: 6, b: 14, operation: {(z: Int, w: Int) ->  Int in
    return (z + w) * 2
})
