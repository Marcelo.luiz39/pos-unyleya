import 'package:flutter/material.dart';
import 'package:tela_login/view/signup_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Nosso Evento',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.deepPurple,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(50.0),
          child: Form(
            child: Column(
              children: [
                Flexible(
                  flex: 1,
                  child: Image.asset('assets/images/logo.png',
                      width: 150.0, height: 150.0),
                ),
                Flexible(
                  child: TextFormField(
                    maxLength: 100,
                    decoration: const InputDecoration(hintText: "E-mail"),
                    controller: _emailController,
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) => value == "" ? value : null,
                  ),
                ),
                Flexible(
                  child: TextFormField(
                    maxLength: 20,
                    obscureText: true,
                    decoration: const InputDecoration(hintText: "Senha"),
                    controller: _passwordController,
                    keyboardType: TextInputType.text,
                    validator: (value) => value == "" ? value : null,
                  ),
                ),
                Flexible(
                  child: TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.deepPurple,
                    ),
                    onPressed: () {
                      _login(context);
                    },
                    child: const Text('Entrar',
                        style: TextStyle(color: Colors.white)),
                  ),
                ),
                Flexible(
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const SignupScreen(),
                      ));
                    },
                    child: const Text('Cadastrar',
                        style: TextStyle(
                          color: Colors.deepPurple,
                          decoration: TextDecoration.underline,
                        )),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  
  void _login(BuildContext context) {}
}
