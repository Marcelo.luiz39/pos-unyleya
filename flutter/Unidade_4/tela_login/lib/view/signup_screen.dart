import 'package:flutter/material.dart';

class SignupScreen extends StatefulWidget {
  const SignupScreen({super.key});

  @override
  State<SignupScreen> createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Nosso Evento',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.deepPurple,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(50.0),
          child: Form(
            child: Column(
              children: [
                Flexible(
                  flex: 1,
                  child: Image.asset('assets/images/logo.png',
                      width: 150.0, height: 150.0),
                ),
                Flexible(
                  child: TextFormField(
                    maxLength: 100,
                    decoration: const InputDecoration(hintText: "Nome"),
                    controller: _nameController,
                    keyboardType: TextInputType.name,
                    validator: (value) => value == "" ? value : null,
                  ),
                ),
                Flexible(
                  child: TextFormField(
                    maxLength: 100,
                    decoration: const InputDecoration(hintText: "E-mail"),
                    controller: _emailController,
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) => value == "" ? value : null,
                  ),
                ),
                Flexible(
                  child: TextFormField(
                    maxLength: 20,
                    obscureText: true,
                    decoration: const InputDecoration(hintText: "Senha"),
                    controller: _passwordController,
                    keyboardType: TextInputType.text,
                    validator: (value) => value == "" ? value : null,
                  ),
                ),
                Flexible(
                  child: TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.deepPurple,
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const SignupScreen(),
                      ));
                    },
                    child: const Text('Cadastrar',
                        style: TextStyle(
                          color: Colors.white,
                        )),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
