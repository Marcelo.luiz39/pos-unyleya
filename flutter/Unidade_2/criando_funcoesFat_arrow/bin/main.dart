void main() {
  int valor = 0;
  //função fat arrow
  void alterarValor(int novoValor) => valor = novoValor;

  String nome = 'Marcelo';
  String novoNome() => nome;

  var soma = (a, b) => a + b;

  var multiplicar = (num a, num b) => a * b;

  String imprimir() => 'Marcelo Luiz';

  alterarValor(3);

  print("$valor");
  print("${novoNome()}");
  print(soma(2, 7));
  print(multiplicar(2, 6));
  print(imprimir());

  print(ClasseB.nomes());
}
//função com escopo léxico

class ClasseA {
  static var nome = 'Marcelo Luiz P. Souza';
  static var nomeUp = () {
    return nome.toUpperCase();
  };
}

class ClasseB {
  static String nomes() {
    return ClasseA.nomeUp();
  }
}
