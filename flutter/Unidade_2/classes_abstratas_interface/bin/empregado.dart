import 'Pessoa.dart';

class Empregado extends Pessoa {
  String nome;
  String sobrenome;

  Empregado(this.nome, this.sobrenome);

  String nomeCompleto() {
    return nome + ' ' + sobrenome;
  }
}