import 'monta_calculadora.dart';

class jogaDados {
  int pontos_jogador1 = 10;
  int? pontos_jogador2;
  int pontos_jogador3 = 17;

  jogaDados();

  int totalDados(int dado1, int dado2) {
    return MontaCalculadora().somarGlobal(dado1, dado2);
  }

  int pontosDados(int dados) {
    if (dados <= 6) {
      return dados - 1;
    } else if (dados > 6 && dados < 12) {
      return dados + 5;
    } else if (dados == 12) {
      return dados + 7;
    } else {
      return dados;
    }
  }

  bool ehVencedor(int totalJogador) {
    return totalJogador >= 15 ? true : false;
  }

  int totalPontosJogadores(int totalJogador) {
    int i = 0;
    int a = pontos_jogador1;
    int b = pontos_jogador2 ?? i; // se for nulo, recebe 0
    int c = pontos_jogador3;

    return a + b + c;
  }

  int newMethod(int i) => i;

  String pegaCarta(String carta) {
    String mensagem = "";

    switch (carta) {
      case "AGIR":
        {
          mensagem = "Avance 2 casas";
        }
        break;
      case "DOAR":
        {
          mensagem = "Passe a vez";
        }
        break;
      default:
        {
          mensagem = "Carta inválida";
        }
        break;
    }

    return mensagem;
  }

   jogadasFor() {
    for (int i = 1; i <= 10; i++) {
      print("$i");
    }
  }

  void jogadasInForIn() {
    for( String vez in ["jogador1", "jogador2", "jogador3"]){
      print("jogador da vez: $vez");
    }
  }

  void jogadasInWhile() {
    int i = 1;
    while (i <= 9) {
      print("$i - Jogada");
      i++;
    }
  }

  String jogadasInDoWhile() {
    int i = 1;
    do {
      print("$i - Jogada");
      i++;
    } while (i <= 9);
    return "Continua";
  }

  String jogadasInDoWhile2() {
    int i = 1;
    while (i <= 9){
      print("$i");
      i++;
    }
    return "Fim de jogo";
    
  }
}
