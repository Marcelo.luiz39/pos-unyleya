// import 'monta_calculadora.dart';
import 'joga_dados.dart';

void main() {
  /*var name = 'Dart';

  print('Hello, $name!');
  print('Novo projeto Dart!');
  print('A soma é: ${MontaCalculadora().somaGlobal(5, 6)}');
  print('A subtração é: ${MontaCalculadora().subtraiGlobal()}');
  print('A divisão é: ${MontaCalculadora().divisaoGlobal()}');
  print(
      'A multiplicação sem parâmetros é: ${MontaCalculadora().multiplicaGlobal()}');
  print(
      'A multiplicação com parâmetros é: ${MontaCalculadora().multiplica(5, 8)}');
  print(
      'A soma com parâmetros em double é: ${MontaCalculadora().soma(5.5, 7.1)}');
  print(
      'A subtração com parâmetros em double e int é: ${MontaCalculadora().subtrai(15.5, 7)}');
  print(
      'A divisão com parâmetros em num é: ${MontaCalculadora().dividirNum(20.5, 4)}');
  print(
      'O primeiro número é maior que o segundo? ${MontaCalculadora().maiorQue(20, 4)}');
  print(
      'O valor passado por parâmetro é nulo ? ${MontaCalculadora().isValueNull(2)}');
  print('O valor passado e um inteiro ? ${3.1 is int }');
  print('Valor passado e um ponto flutuante ? ${3.1 is double }');
  print('Falso e um booleano ? ${false is bool }');
  print('Valor e diferente de int ? ${3.1 is! int }');
  print('Valor passado e uma string ? ${'projeto' is String }'); // aqui ja inferiu o tipo, por isso o warning*/

  print("Resultado: ${jogaDados().totalDados(3, 7)}");
  print("Resultado: ${jogaDados().pontosDados(3)}");
  print("Resultado: ${jogaDados().pontosDados(6)}");
  print("Resultado: ${jogaDados().pontosDados(8)}");
  print("Resultado: ${jogaDados().pontosDados(12)}");
  print("Resultado: ${jogaDados().pontosDados(0)}");
  print("Resultado: ${jogaDados().ehVencedor(5)}");
  print("Resultado: ${jogaDados().totalPontosJogadores(5)}");
  print("Resultado: ${jogaDados().pegaCarta("AGIR")}");
  print("Resultado: ${jogaDados().pegaCarta("DOAR")}");
  print("Resultado: ${jogaDados().pegaCarta("NÃO")}");
  jogaDados().jogadasFor();
  print("----------");
  jogaDados().jogadasInForIn();
  print("----------");
  jogaDados().jogadasInWhile();
  print("----------");
  print("Resultado: ${jogaDados().jogadasInDoWhile()}");
  print("----------");
  print("Resultado: ${jogaDados().jogadasInDoWhile2()}");
}
