void main() {
  Funcionario funcionario = Funcionario();
  print(funcionario.nome);
  print(funcionario.idade);
  print(funcionario.salario);
  print(funcionario.contaAtiva);

  funcionario.registrarPonto();
}

class Funcionario {
  String nome = "Marcelo Luiz";
  int idade = 43;
  double salario = 4.667;
  bool contaAtiva = true;

  void registrarPonto() {
    print(DateTime.now());
  }
}
