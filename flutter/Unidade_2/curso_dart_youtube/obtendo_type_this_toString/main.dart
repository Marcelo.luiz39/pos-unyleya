class Produto {
  // utilizando o this para obter o type
  @override
  String toString() {
    return 'Instancia de: ${this.runtimeType}';
  }
}

void main() {
  // formas de obter o type
  // Type type = Produto;
  // print(type.toString());
  //print(Produto.toString()); nao disponivel

  // Produto produto = Produto();
  // print(produto.toString());
  
  // var produto = Produto();
  // print(produto is Produto); aqui ele ja sabe o typo e da um warning

  var produto = getInstance();
  print(produto is Produto);
}

Object getInstance() => Produto();
