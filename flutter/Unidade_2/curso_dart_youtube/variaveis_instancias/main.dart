// formas de inicializar variáveis de instância
class Cliente {
  /* 
  aqui da erro PQ nao foi inicializado 
   String nome;
   String email;
   */

  //aqui nao da erro PQ foi inicializado com um valor vazio
  String nome = '';

  //aqui nao da erro PQ foi colocado o ? que significa que pode ser nulo
  String? sobrenome;

//aqui nao da erro PQ foi colocado o late que significa que pode ser nulo
  late String email;

  //aqui nao da erro PQ foi colocado o final que significa que pode ser nulo porem nao pode ser alterado
  late final String cpf;

  //aqui nao da erro PQ foi colocado o usado o construtor
  Cliente(this.nome, this.sobrenome, this.cpf);

}

void main() {
  var cliente = Cliente('Marcelo', 'Santos', '123.456.789-00');
  cliente.nome = 'Marcelo';
  // cliente.cpf = '123.456.789-00'; aqui da erro pq o cpf nao pode ser alterado
  cliente.email = 'marcelo@email.com';
  cliente.sobrenome = 'Souza';
  print(cliente.nome);
  print(cliente.sobrenome);
  print(cliente.email);
  print(cliente.cpf);
}