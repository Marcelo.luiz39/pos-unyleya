class MontaCalculadora {
  int a = 16;
  int b = 2;

  MontaCalculadora();

  int somaGlobal(int dado1, int dado2) {
    return a + b;
  }

  int subtraiGlobal() {
    return a - b;
  }

  int divisaoGlobal() {
    return a ~/ b;
  }

  int multiplicaGlobal() {
    return a * b;
  }

  int multiplica(int c, int d) {
    return c * d;
  }

  int somarGlobal(int value1, int value2) {
    return value1 + value2;
  }

  double soma(double c, double d) {
    return c + d;
  }

  double subtrai(double c, int d) {
    return c - d;
  }

  num dividirNum(num c, num d) {
    return c / d;
  }

  bool maiorQue(int c, int d) {
    return c > d;
  }

  bool isValueNull(dynamic c) {
    return c == Null;
  }
}
