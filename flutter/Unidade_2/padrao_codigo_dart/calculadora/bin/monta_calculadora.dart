class MontaCalculadora {
  int a = 16;
  int b = 2;

  MontaCalculadora() {
    print('Construtor da classe MontaCalculadora mais o valor $a');
  }

  int somaGlobal() {
    return a + b;
  }

  int divisaoGlobal() {
    return a ~/ b;
  }
}
