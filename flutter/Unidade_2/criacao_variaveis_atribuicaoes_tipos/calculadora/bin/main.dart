import 'monta_calculadora.dart';

void main() {
  var name = 'Dart';

  print('Hello, $name!');
  print('Novo projeto Dart!');
  print('A soma é: ${MontaCalculadora().somaGlobal()}');
  print('A subtração é: ${MontaCalculadora().subtraiGlobal()}');
  print('A divisão é: ${MontaCalculadora().divisaoGlobal()}');
  print(
      'A multiplicação sem parâmetros é: ${MontaCalculadora().multiplicaGlobal()}');
  print(
      'A multiplicação com parâmetros é: ${MontaCalculadora().multiplica(5, 8)}');
  print(
      'A soma com parâmetros em double é: ${MontaCalculadora().soma(5.5, 7.1)}');
  print(
      'A subtração com parâmetros em double e int é: ${MontaCalculadora().subtrai(15.5, 7)}');
  print(
      'A divisão com parâmetros em num é: ${MontaCalculadora().dividirNum(20.5, 4)}');
  print(
      'O primeiro número é maior que o segundo? ${MontaCalculadora().maiorQue(20, 4)}');
  print(
      'O valor passado por parâmetro é nulo ? ${MontaCalculadora().isValueNull(2)}');
  print('O valor passado e um inteiro ? ${3.1 is int }');
  print('Valor passado e um ponto flutuante ? ${3.1 is double }');
  print('Falso e um booleano ? ${false is bool }');
  print('Valor e diferente de int ? ${3.1 is! int }');
  print('Valor passado e uma string ? ${'projeto' is String }'); // aqui ja inferiu o tipo, por isso o warning
}
