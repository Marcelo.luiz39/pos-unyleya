// usando o @override para sobrescrever o método toString() da classe Object
// trabalhando com herança e construtores

class Pessoa {
  String nome;
  int idade;
  double altura;
  double peso;

  Pessoa(this.nome, this.idade, this.altura, this.peso);

  @override
  String toString() {
    return "Nome: $nome, Idade: $idade, Altura: $altura, Peso: $peso";
  }
}

class Empregado extends Pessoa {
  double salario;

  Empregado(String nome, int idade, double altura, double peso, this.salario)
      : super(nome, idade, altura, peso);

  @override
  String toString() {
    return "Nome: $nome, Idade: $idade, Altura: $altura, Peso: $peso, Salário: $salario";
  }
}

class Estudante extends Pessoa with Anda {
  late num media;

  Estudante(String nome, int idade, double altura, double peso, num notas)
      : super(nome, idade, altura, peso) {
    this.media = notas / 3;
  }
}

class Ave {
  late String nome;
}

class Mamifero {
  late String nome;
}

/* aqui a baixo eu consigo retornar uma String na diretamente na chamada
do método dentro do print */
mixin Anda {
  String andando() => "ele está Andando";
}


/* Aqui eu não consigo retornar uma String na chamada do método
porem eu consigo imprimir uma String na chamada do método */
mixin Nada {
  void nadando() {
    print("ele está Nadando !!!");
  }
}

mixin Voa {
  voando() {
    print("ele está Voando !!!");
  }
}

class Pato extends Ave with Voa, Anda {}

class Morcego extends Mamifero with Voa {}

class Golfinho extends Mamifero with Nada {}

void main() {
  Pessoa pessoa = Pessoa("João", 30, 1.80, 80.0);
  Empregado empregado = Empregado("Maria", 25, 1.70, 60.0, 1000.0);
  Estudante estudante = Estudante("Pedro", 20, 1.60, 50.0, 9.5);

  print("Nome: ${pessoa.nome}");
  print("Idade: ${pessoa.idade}");
  print("Altura: ${pessoa.altura}");
  print("Peso: ${pessoa.peso}");

  print("--------------------");

  print("Nome do funcionário: ${empregado.nome}");
  print("Idade: ${empregado.idade}");
  print("Altura: ${empregado.altura}");
  print("Peso: ${empregado.peso}");
  print("Salário: ${empregado.salario}");

  print("--------------------");

  print("Nome do estudante: ${estudante.nome}");
  print("Idade: ${estudante.idade}");
  print("Altura: ${estudante.altura}");
  print("Peso: ${estudante.peso}");
  print("Média: ${estudante.media.ceil()}");
  print("""
${estudante.nome} ${estudante.andando()} até a escola.""");

  print("--------------------");

  Morcego morcego = Morcego();
  Pato pato = Pato();

  morcego.nome = "Batman";
  pato.nome = "Donald";

  print("Nome do Morcego: ${morcego.nome}");
  morcego.voando();
  print(pato.andando());
  print("Nome do Pato: ${pato.nome} ${pato.andando()} !!!");
}
