//exemplo de construtor com parâmetros opcionais

class ClasseA {
  String? nome;
  int? idade;

//Com parâmetros opcionais
  ClasseA({nome, idade}) {
    this.nome = nome;
    this.idade = idade;
  }

  //Construtores nomeados
  ClasseA.maiusculas({nome, idade}) {
    this.nome = nome.toUpperCase();
    this.idade = idade;
  }

  //Exemplos com getters e setters

  //GETTERS
  String get nome1 => nome!;
  String getNome() {
    return nome!;
  }

  //SETTERS
  void set nome1(novoNome) => this.nome = novoNome;
  void setNome(novoNome) {
    this.nome = novoNome;
  }
}

class ClasseB {
  ClasseA classe1 = ClasseA(nome: 'Marcelo Luiz', idade: 43);

  ClasseA classe2 = ClasseA();

  ClasseA classe3 = ClasseA.maiusculas(nome: 'Marcelo Luiz', idade: 43);
}

//Exemplo de classe estática

//Sintaxe: type methodName([parameters]) => expression;
class ClasseC {
  static int gerarNumero() => 5;
}

class ClasseD {
  int numero = ClasseC.gerarNumero();
}

//Diferença entre final e const

//final: pode ser declarado em tempo de execução e somente uma vez em cada instância.
//const: deve ser declarado em tempo de compilação e é uma constante em tempo de execução

//Exemplo de classe com final e const
class ClasseE {
  static const String urlBase = 'https://www.google.com.br';
  final String corApp;
  ClasseE(this.corApp);
}

class ClasseF {
  ClasseE classe1 = ClasseE('azul');
  //ClasseE classeE = ClasseE('azul'.toUpperCase()); //ERRO
  ClasseE classe2 = ClasseE('verde');
  String url = ClasseE.urlBase;
  String urlMaps = ClasseE.urlBase + '/maps';
}
