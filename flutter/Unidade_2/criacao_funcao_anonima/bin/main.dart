void main() {
  int valor = 0;

//função padrão
  void alteraValor(int novoValor) {
    valor = novoValor;
  }

  int somaValores(int valor1, int valor2) {
    return valor1 + valor2;
  }

  String nomeCompleto(String nome, String sobrenome) {
    return '$nome $sobrenome';
  }

  //função anônima
  var somaValorAnonima = (int valor1, int valor2) {
    return valor1 + valor2;
  };

  var funcaoAnonima = (String nome, String sobrenome) {
    return '$nome $sobrenome';
  };

  //função dinâmica
  var somaValorDinamica = (valor1, valor2) => valor1 + valor2;

  alteraValor(3);
  print("$valor");

  print("${somaValores(2, 3)}");

  print("${nomeCompleto("Marcelo", "Luiz")}");

  print("${somaValorAnonima(5, 2)}");

  print("${funcaoAnonima("Julia", "Souza")}");

  print("${somaValorDinamica(6, 3)}");


}
