void main() {
  //parâmetros opcionais
  int somaElementos({a, b, c}) {
    int x = a ?? 0;
    int y = b ?? 0;
    int z = c ?? 0;
    return x + y + z;
  }

  print(somaElementos(a: 2, c: 5));

  int somaDoisElementos({a}) {
    int x = a ?? 0;
    return x + 2;
  }

  print(somaDoisElementos());
  print("${somaDoisElementos(a: 3)}");

  //parâmetros posicionais

  String civil(String nome, String idade, [casado = "Não"]) {
    return "$nome tem $idade anos e é Casado: $casado";
  }

  print(civil("Marcelo Luiz", "43"));
}
