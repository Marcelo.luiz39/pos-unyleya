class ClasseA {
  List lista = [];

  List adicionar() {
    lista.add("A");
    lista.add("B");
    lista.add("C");

    return lista;
  }
}

void main() {
  ClasseA classeA = ClasseA();

  print(classeA.adicionar());
  bool valorAtivo = true;
  List<dynamic> lista = [];
  print(lista);

  List lista1 = [1, 2, 3];
  print(lista1);

  List lista2 = [4, 5, 6];
  print(lista2);

  var lista3 = null;

  var listaTotal = [];
  print("Lista vazia: $listaTotal");

  // var listaTotal = lista1 + lista2;
  // listaTotal.addAll(lista2);
  // listaTotal = [...lista1];
  listaTotal = [...lista1, ...lista2];
  print("Lista total com spreed: $listaTotal");

  listaTotal = [0, ...lista1, ...lista2];
  print("Lista total com spreed 0: $listaTotal");

  var listaTotal1 = [...lista1, ...lista2, ...?lista3];
  print("Lista total 1 com spreed null: $listaTotal1");

  var listaTotal2 = [listaTotal, 7, 8, 9];
  print("Lista total com lista e novos itens: $listaTotal2");

  print("Tamanho listaTotal 2: $listaTotal2");

  List listaTotal3 = [...lista1, ...lista2, 7, 8, 9, if (valorAtivo) 10];
  print("Lista total 3 com spreed e if: $listaTotal3");
}
