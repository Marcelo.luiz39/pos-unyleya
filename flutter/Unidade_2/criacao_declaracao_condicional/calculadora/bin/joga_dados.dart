import 'monta_calculadora.dart';

class jogaDados {
  int pontos_jogador1 = 10;
  int pontos_jogador2 = 0;
  int pontos_jogador3 = 17;

  jogaDados();

  int totalDados(int dado1, int dado2) {
    return MontaCalculadora().somarGlobal(dado1, dado2);
  }

  int pontosDados(int dados) {
    if (dados <= 6) {
      return dados - 1;
    } else if (dados > 6 && dados < 12) {
      return dados + 5;
    } else if (dados == 12) {
      return dados + 7;
    } else {
      return dados;
    }
  }
}
