import 'package:flutter/material.dart';

import '../services/requisicao.dart';

class CidadesDados extends StatefulWidget {
  const CidadesDados({super.key});

  @override
  State<CidadesDados> createState() => _CidadesDadosState();
}

class _CidadesDadosState extends State<CidadesDados> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FutureBuilder(
            future: Requisicao.requisicaoCidades(),
            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              List? cidades = snapshot.data;
              return _listarCidades(cidades);
            }),
      ],
    );
  }
}
