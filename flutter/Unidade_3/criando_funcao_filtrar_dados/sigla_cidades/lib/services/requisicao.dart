import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Requisicao {
  static Future<List> requisicaoCidades() async {
    var cidades = {};
    String url = "http://api.nobelprize.org/v1/country.json";
  
    http.Response response = await http.get(Uri.parse(url));
    debugPrint(response.body);

    if (response.statusCode == 200) {
      cidades = json.decode(response.body);

      return cidades.values.first;
    } else {
      throw Exception("Falha na Requisição!");
    }
  }
}
