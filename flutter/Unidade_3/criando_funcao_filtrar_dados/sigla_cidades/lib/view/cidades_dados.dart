import 'package:flutter/material.dart';

import '../services/requisicao.dart';

class CidadesDados extends StatefulWidget {
  final String estado;
  const CidadesDados({Key? key, required this.estado}) : super(key: key);

  @override
  State<CidadesDados> createState() => _CidadesDadosState();
}

class _CidadesDadosState extends State<CidadesDados> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FutureBuilder(
          future: Requisicao.requisicaoCidades(),
          builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            List? estados = snapshot.data;

            return _listarCidades(
                _filtrarEstados(estados, widget.estado, context));
          },
        ),
      ],
    );
  }

  Widget _listarCidades(List? estados) {
    return Expanded(
      child: ListView.builder(
        itemCount: estados!.length,
        itemBuilder: (context, index) {
          var cidade = estados[index];
          return Card(
            child: ExpansionTile(
              title: Text(
                cidade['nome'],
                style: const TextStyle(
                  color: Colors.blueAccent,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  padding: const EdgeInsets.all(20),
                  child: Text(
                    cidade['sigla'],
                    style: const TextStyle(
                      color: Colors.grey,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  List? _filtrarEstados(List? estados, String estado, BuildContext context) {
    var listaFiltrada = <dynamic>[];

    for (var i = 0; i < estados!.length; i++) {
      if (estados[i]['nome'].toString().contains(estado)) {
        listaFiltrada.add(estados[i]);
       
      }
    }

    return listaFiltrada.isEmpty ? estados : listaFiltrada;
  }
}
