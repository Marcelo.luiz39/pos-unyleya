import 'package:flutter/material.dart';
import 'package:sigla_cidades/view/cidades_screen.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Sigla das Cidades',
      home: CidadesScreen(),
    );
  }
}
