import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Requisicao {
  static Future<List> requisicaoCidades() async {
    var cidades = <dynamic>[];
    String url =
        "https://servicodados.ibge.gov.br/api/v1/localidades/estados";
    // String url = "http://api.nobelprize.org/v1/country.json";

    http.Response response = await http.get(Uri.parse(url));
    debugPrint("Resultado: ${response.body}");

    if (response.statusCode == 200) {
      cidades = json.decode(response.body);

      return cidades;
    } else {
      throw Exception("Falha na Requisição!");
    }
  }
}
