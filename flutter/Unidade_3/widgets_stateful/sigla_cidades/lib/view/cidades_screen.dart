import 'package:flutter/material.dart';
import 'package:sigla_cidades/services/requisicao.dart';

import 'cidades_dados.dart';
import 'menu.dart';

class CidadesScreen extends StatelessWidget {
  const CidadesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Sigla das Cidades"),
        actions: [
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: () {
              Requisicao.requisicaoCidades();
            },
          )
        ],
      ),
      body: const CidadesDados(),
      drawer: const Menu(),
    );
  }
}
