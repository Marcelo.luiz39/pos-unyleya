import 'package:flutter/material.dart';
import 'package:sigla_cidades/services/requisicao.dart';

class CidadesScreen extends StatelessWidget {
  const CidadesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Sigla das Cidades"),
        actions: [
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: () {
              Requisicao.requisicaoCidades();
            },
          )
        ],
      ),
      body: Container(
        alignment: Alignment.center,
        child: TextButton(
          child: const Text("Clique para ver as cidades"),
          onPressed: () {
            Requisicao.requisicaoCidades();
          },
        ),
      ),
    );
  }
}
