import 'package:flutter/material.dart';

import 'cidades_dados.dart';
import 'menu.dart';

class CidadesScreen extends StatelessWidget {
  const CidadesScreen({super.key});


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text("Sigla dos Estados"),
        actions: [
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: () {
             
            },
          )
        ],
      ),
      body: const CidadesDados(),
      drawer: const Menu(),
    );
  }
}
