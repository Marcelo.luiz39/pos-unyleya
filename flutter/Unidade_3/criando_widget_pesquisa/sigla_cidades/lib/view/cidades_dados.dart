import 'package:flutter/material.dart';

import '../services/requisicao.dart';

class CidadesDados extends StatefulWidget {
  const CidadesDados({super.key});

  @override
  State<CidadesDados> createState() => _CidadesDadosState();
}

class _CidadesDadosState extends State<CidadesDados> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FutureBuilder(
            future: Requisicao.requisicaoCidades(),
            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              List? estados = snapshot.data;

              return _listarCidades(estados);
            }),
      ],
    );
  }

  Widget _listarCidades(List? estados) {
    return Expanded(
      child: ListView.builder(
        itemCount: estados!.length,
        itemBuilder: (context, index) {
          return Card(
            child: ExpansionTile(
              title: Text(
                estados[index]['nome'],
                style: const TextStyle(
                    color: Colors.blueAccent,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  padding: const EdgeInsets.all(20),
                  child: Text(estados[index]['sigla'],
                      style: const TextStyle(
                          color: Colors.grey,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
