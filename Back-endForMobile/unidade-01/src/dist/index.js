"use strict";
/**
 
Singleton
 
 * A classe Singleton é uma classe que só pode existir uma vez, definindo os métodos de inicialização e destruição de uma classe.
 * Usando o método `getInstance` que permite aos clientes acessar a instância de uma classe Singleton
 */
class Singleton {
    /**
     * O construtor não pode ser acessado fora da classe Singleton.
     * Usamos ele como privado, para não permitir a instanciação com o uso do operador `new`.
     */
    constructor() { }
    /**
     *
     * @returns O método estático que controla o acesso a instância Singleton
     * Esta implementação permite que voce crie uma subclasse de Singleton enquanto
     * mantém apenas uma instância de cada subclasse.
     */
    static getInstance() {
        if (!Singleton.instance) {
            Singleton.instance = new Singleton();
        }
        return Singleton.instance;
    }
    /**
     * Finalmente, qualquer singleton deve definir alguma logica de inicialização, que pode ser executada apenas uma vez em cada instanciação.
     */
    someBusinessLogic() { }
}
/**
 * The client code.
 */
function clientCodeSingleton() {
    const s1 = Singleton.getInstance();
    const s2 = Singleton.getInstance();
    if (s1 === s2) {
        console.log('Singleton works, both variables contain the same instance.');
    }
    else {
        console.log('Singleton failed, variables contain different instances.');
    }
}
clientCodeSingleton();
console.log('');
/*

FunctionMethod

A classe Creator declara o método de fabrica que
deve retornar um objeto de uma classe de produto.
As subclasses do criador geralmente fornecem a implementação
dos métodos de fabrica.
*/
class Creator {
    /*
   Observe também que, apesar do nome, a principal responsabilidade
   do Criador é nao criar objetos, mas sim fornecer a implementação
   de produto.
   Normalmente contem alguma logica de negocio central que depende de
   objetos Product, retornados pela método de fabrica. Subclasses podem
   alterar indiretamente essa logica de negócios substituindo a
   implementação de fabrica e devolvendo um tipo diferente de Product.
   */
    someOperation() {
        // chame o método de fabrica para criar o objeto
        const product = this.factoryMethod();
        return `Criador: o mesmo código do criador acaba de 
    trabalhar com ${product.operation()}`;
    }
}
/*
Os criadores concretos substituem a implementação de fabrica para alterar
a logica de negócios e o tipo de objeto.
*/
class ConcreteCreator1 extends Creator {
    /**
     *
     * Observe que a assinatura do método ainda usa o
     * produto abstrato tipo Product, mesmo que o criador
     * substitua a implementação de fabrica, dessa forma o criador
     * não precisa implementar o método de fabrica e pode permanecer
     * independente do produto concreto.
     */
    factoryMethod() {
        return new ConcreteProduct1();
    }
}
class ConcreteCreator2 extends Creator {
    factoryMethod() {
        return new ConcreteProduct2();
    }
}
/**
 * Os Products concretos implementam a interface Product
 * e fornecem varias implementações.
 */
class ConcreteProduct1 {
    operation() {
        return 'ConcreteProduct1';
    }
}
class ConcreteProduct2 {
    operation() {
        return 'ConcreteProduct2';
    }
}
/**
 *
 * O código do cliente funciona como uma instancia de um
 * criador concreto, embora por meio de sua interface base.
 * Contanto que o cliente continue trabalhando com o criador via a
 * interface base, voce pode passar para qualquer subclasse do criador.
 */
function clientCodeFactorMethod(creator) {
    console.log("Client: I/m not aware of the creator's class, \n" + 'but it still works.');
    console.log(creator.someOperation());
}
console.log('App: Launched with the ConcreteCreator1.');
clientCodeFactorMethod(new ConcreteCreator1());
console.log('');
console.log('App: Launched with the ConcreteCreator2.');
clientCodeFactorMethod(new ConcreteCreator2());
console.log('');
/**
  Prototype
  
 A classe de exemplo que possui capacidade de clonagem. Vamos ver
 como os valores de campo com tipos diferentes a serem clonados.
 */
class Prototype {
    clone() {
        const clone = Object.create(this);
        clone.component = Object.create(this.component);
        clone.circularReference = Object.assign(Object.assign({}, this.circularReference), { Prototype: Object.assign({}, this) });
        return clone;
    }
}
class componentWithBackReference {
    constructor(prototype) {
        this.prototype = prototype;
    }
}
/**
 * The client code
 */
function clientCodePrototype() {
    const p1 = new Prototype();
    p1.primitive = 245;
    p1.component = new Date();
    p1.circularReference = new componentWithBackReference(p1);
    const p2 = p1.clone();
    if (p1.primitive === p2.primitive) {
        console.log('Primitive field values have been carried over to a clone. Yay!');
    }
    else {
        console.log('Primitive field values have not been copied. Booo!');
    }
    if (p1.component === p2.component) {
        console.log('Simple component has not been cloned. Booo!');
    }
    else {
        console.log('Simple component has been cloned. Yay!');
    }
    if (p1.circularReference === p2.circularReference) {
        console.log('Circular reference has not been cloned. Booo!');
    }
    else {
        console.log('Circular reference has been cloned. Yay!');
    }
    if (p1.circularReference.prototype === p2.circularReference.prototype) {
        console.log('Circular reference\'s prototype has not been cloned. Booo!');
    }
    else {
        console.log('Circular reference\'s prototype has been cloned. Yay!');
    }
}
clientCodePrototype();
