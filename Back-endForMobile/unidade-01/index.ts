/**
 
Singleton
 
 * A classe Singleton é uma classe que só pode existir uma vez, definindo os métodos de inicialização e destruição de uma classe.
 * Usando o método `getInstance` que permite aos clientes acessar a instância de uma classe Singleton
 */

class Singleton {
  private static instance: Singleton

  /**
   * O construtor não pode ser acessado fora da classe Singleton.
   * Usamos ele como privado, para não permitir a instanciação com o uso do operador `new`.
   */

  private constructor() {}

  /**
   *
   * @returns O método estático que controla o acesso a instância Singleton
   * Esta implementação permite que voce crie uma subclasse de Singleton enquanto
   * mantém apenas uma instância de cada subclasse.
   */

  public static getInstance(): Singleton {
    if (!Singleton.instance) {
      Singleton.instance = new Singleton()
    }

    return Singleton.instance
  }
  /**
   * Finalmente, qualquer singleton deve definir alguma logica de inicialização, que pode ser executada apenas uma vez em cada instanciação.
   */

  public someBusinessLogic() {}
}

/**
 * The client code.
 */

function clientCodeSingleton() {
  const s1 = Singleton.getInstance()
  const s2 = Singleton.getInstance()

  if (s1 === s2) {
    console.log('Singleton works, both variables contain the same instance.')
  } else {
    console.log('Singleton failed, variables contain different instances.')
  }
}

clientCodeSingleton()
console.log('')

/*

FunctionMethod

A classe Creator declara o método de fabrica que 
deve retornar um objeto de uma classe de produto. 
As subclasses do criador geralmente fornecem a implementação 
dos métodos de fabrica.
*/

abstract class Creator {
  /*
  Observe que o Criador também pode fornecer a 
  implementação dos métodos de fabrica.
  */
  public abstract factoryMethod(): Product

  /*
 Observe também que, apesar do nome, a principal responsabilidade
 do Criador é nao criar objetos, mas sim fornecer a implementação
 de produto.
 Normalmente contem alguma logica de negocio central que depende de 
 objetos Product, retornados pela método de fabrica. Subclasses podem
 alterar indiretamente essa logica de negócios substituindo a 
 implementação de fabrica e devolvendo um tipo diferente de Product.
 */

  public someOperation(): string {
    // chame o método de fabrica para criar o objeto
    const product = this.factoryMethod()
    return `Criador: o mesmo código do criador acaba de 
    trabalhar com ${product.operation()}`
  }
}

/* 
Os criadores concretos substituem a implementação de fabrica para alterar
a logica de negócios e o tipo de objeto.
*/

class ConcreteCreator1 extends Creator {
  /**
   *
   * Observe que a assinatura do método ainda usa o
   * produto abstrato tipo Product, mesmo que o criador
   * substitua a implementação de fabrica, dessa forma o criador
   * não precisa implementar o método de fabrica e pode permanecer
   * independente do produto concreto.
   */
  public factoryMethod(): Product {
    return new ConcreteProduct1()
  }
}

class ConcreteCreator2 extends Creator {
  public factoryMethod(): Product {
    return new ConcreteProduct2()
  }
}

/**
 * A interface Product define o método de fabrica e declara as operações
 * que todos os objetos Product devem implementar.
 */
interface Product {
  operation(): string
}

/**
 * Os Products concretos implementam a interface Product
 * e fornecem varias implementações.
 */

class ConcreteProduct1 implements Product {
  public operation(): string {
    return 'ConcreteProduct1'
  }
}

class ConcreteProduct2 implements Product {
  public operation(): string {
    return 'ConcreteProduct2'
  }
}

/**
 *
 * O código do cliente funciona como uma instancia de um
 * criador concreto, embora por meio de sua interface base.
 * Contanto que o cliente continue trabalhando com o criador via a
 * interface base, voce pode passar para qualquer subclasse do criador.
 */

function clientCodeFactorMethod(creator: Creator) {
  console.log(
    "Client: I/m not aware of the creator's class, \n" + 'but it still works.'
  )
  console.log(creator.someOperation())
}

console.log('App: Launched with the ConcreteCreator1.')
clientCodeFactorMethod(new ConcreteCreator1())
console.log('')

console.log('App: Launched with the ConcreteCreator2.')
clientCodeFactorMethod(new ConcreteCreator2())
console.log('')

/**
  Prototype
  
 A classe de exemplo que possui capacidade de clonagem. Vamos ver 
 como os valores de campo com tipos diferentes a serem clonados.
 */

class Prototype {
  public primitive: any
  public component: object | any
  public circularReference: componentWithBackReference | any

  public clone(): this {
    const clone = Object.create(this)

    clone.component = Object.create(this.component)

    /**
     * Clonando um objeto que tem um objeto aninhado com referencia anterior
     * requer tratamento especial. Depois que a clonagem for concluída, o objeto
     * aninhado deve apontar para o objeto clonado, em vez do objeto original.
     * O operador new cria um objeto aninhado e o aponta para o objeto clonado.
     * O operador Spread Operator cria um objeto aninhado e o aponta para o objeto. 
     */

    clone.circularReference = {
      ...this.circularReference,
      Prototype: { ...this },
    }
    return clone
  }
}

class componentWithBackReference {
  public prototype
  

  constructor(prototype: Prototype) {
    this.prototype = prototype
  }
}

/**
 * The client code
 */

function clientCodePrototype() {
  const p1 = new Prototype()
  p1.primitive = 245
  p1.component = new Date()
  p1.circularReference = new componentWithBackReference(p1)

  const p2 = p1.clone()
  if (p1.primitive === p2.primitive) {
    console.log('Primitive field values have been carried over to a clone. Yay!')
  } else {
    console.log('Primitive field values have not been copied. Boo!')
  }

  if (p1.component === p2.component) {
    console.log('Simple component has not been cloned. Boo!')
  } else {
    console.log('Simple component has been cloned. Yay!')
  }

  if (p1.circularReference === p2.circularReference) {
    console.log('Circular reference has not been cloned. Boo!')
  } else {
    console.log('Circular reference has been cloned. Yay!')
  }

  if (p1.circularReference.prototype === p2.circularReference.prototype) {
    console.log('Circular reference\'s prototype has not been cloned. Boo!')
  }
  else {
    console.log('Circular reference\'s prototype has been cloned. Yay!')
  }
}

clientCodePrototype()